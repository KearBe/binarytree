package com.kenfogel.binarytree.implementation;

import com.kenfogel.binarytree.comparators.IntComparator;
import java.util.*;

/**
 * Based on the code found at http://cslibrary.stanford.edu/110/BinaryTrees.html
 *
 * @author Ken Fogel
 */
public class BinaryTree<T> {

    // Root node reference. Will be null for an empty tree.
    private BinaryTreeNode root;
    private Comparator comp;

    

    /**
     * Creates an empty binary tree -- a null root reference.
     */
    public BinaryTree(Comparator c) {
        root = null;
        comp = c;
    }

    /**
     * Inserts the given data into the binary tree.Uses a recursive helper.
     *
     * @param data
     */
    public void insert(T data) {
        root = insert(root, data);
    }

    private BinaryTreeNode insert(BinaryTreeNode node, T data) {
        if (node == null) {
            node = new BinaryTreeNode(data);
        } else {
            if (comp.compare(data,node.data) <= 0) {
                node.left = insert(node.left, data);
            } else {
                node.right = insert(node.right, data);
            }
        }

        return (node); // in any case, return the new reference to the caller
    }

    /**
     * Returns true if the given target is in the binary tree.Uses a recursive
     * helper.
     *
     * @param data
     * @return true of false depending on whether the data is found
     */
    public boolean lookup(T data) {
        return (lookup(root, data));
    }

    /**
     * Recursive lookup -- given a node, recur down searching for the given
     * data.
     */
    private boolean lookup(BinaryTreeNode node, T data) {
        if (node == null) {
            return (false);
        }

        if (comp.compare(data, node.data) == 0) {
            return (true);
        } else if (comp.compare(data, node.data) < 0) {
            return (lookup(node.left, data));
        } else {
            return (lookup(node.right, data));
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    /**
     * Returns the number of nodes in the tree. Uses a recursive helper that
     * recurses down the tree and counts the nodes.
     *
     * @return the number of elements in the tree
     */
    public int size() {
        return (size(root));
    }

    private int size(BinaryTreeNode node) {
        if (node == null) {
            return (0);
        } else {
            return (size(node.left) + 1 + size(node.right));
        }
    }

    /**
     * Returns the max root-to-leaf depth of the tree. Uses a recursive helper
     * that recurses down to find the max depth.
     *
     * @return The depth of the tree from the root to the lowest node
     */
    public int maxDepth() {
        return (maxDepth(root));
    }

    private int maxDepth(BinaryTreeNode node) {
        if (node == null) {
            return (0);
        } else {
            int lDepth = maxDepth(node.left);
            int rDepth = maxDepth(node.right);

            // use the larger + 1 
            return (Math.max(lDepth, rDepth) + 1);
        }
    }

    /**
     * Returns the min value in a non-empty binary search tree. Uses a helper
     * method that iterates to the left to find the min value.
     *
     * @return The smallest value in the tree
     */
    public Object minValue() {
        return (minValue(root));
    }

    /**
     * Finds the min value in a non-empty binary search tree.
     */
    private Object minValue(BinaryTreeNode node) {
        BinaryTreeNode current = node;
        while (current.left != null) {
            current = current.left;
        }

        return (current.data);
    }

    /**
     * Prints the node values in the "inorder" order. Uses a recursive helper to
     * do the traversal.
     */
    public void printInorderTree() {
        printInorderTree(root);
        System.out.println();
    }

    private void printInorderTree(BinaryTreeNode node) {
        if (node == null) {
            return;
        }

        // left, node itself, right 
        printInorderTree(node.left);
        System.out.print(node.data + "  ");
        printInorderTree(node.right);
    }

    /**
     * Prints the node values in the "postorder" order. Uses a recursive helper
     * to do the traversal.
     */
    public void printPostorder() {
        printPostorder(root);
        System.out.println();
    }

    private void printPostorder(BinaryTreeNode node) {
        if (node == null) {
            return;
        }

        // first recur on both subtrees 
        printPostorder(node.left);
        printPostorder(node.right);

        // then deal with the node 
        System.out.print(node.data + "  ");
    }

    /**
     * Given a binary tree, prints out all of its root-to-leaf paths, one per
     * line. Uses a recursive helper to do the work.
     */
    public void printPaths() {
        Object[] path = new Object[1000];
        printPaths(root, path, 0);
    }

    /**
     * Recursive printPaths helper -- given a node, and an array containing the
     * path from the root node up to but not including this node, prints out all
     * the root-leaf paths.
     */
    private void printPaths(BinaryTreeNode node, Object[] path, int pathLen) {
        if (node == null) {
            return;
        }

        // append this node to the path array 
        path[pathLen] = node.data;
        pathLen++;

        // it's a leaf, so print the path that led to here 
        if (node.left == null && node.right == null) {
            printArray(path, pathLen);
        } else {
            // otherwise try both subtrees 
            printPaths(node.left, path, pathLen);
            printPaths(node.right, path, pathLen);
        }
    }

    /**
     * Utility that prints ints from an array on one line.
     */
    private void printArray(Object[] ints, int len) {
        int i;
        for (i = 0; i < len; i++) {
            System.out.print(ints[i] + " ");
        }
        System.out.println();
    }

    /**
     * Prints out each level in the tree from left to right. Uses a recursive
     * helper to do the work.
     */
    public void printLineByLine() {
        printLineByLine(root);
    }

    private void printLineByLine(BinaryTreeNode root) {
        int maxLevel = maxLevel(root);

        printNodeInternal(Collections.singletonList(root), 1, maxLevel);
    }
    
    /**
     * prints out all the nodes in a tree format using recursion 
     */
    private <T> void printNodeInternal(List<BinaryTreeNode<T>> nodes, int level, int maxLevel) {
        if (nodes.isEmpty() || isAllElementsNull(nodes)){
            return;
        }

        int floor = maxLevel - level;
        int endgeLines = (int) Math.pow(2, (Math.max(floor - 1, 0)));
        int firstSpaces = (int) Math.pow(2, (floor)) - 1;
        int betweenSpaces = (int) Math.pow(2, (floor + 1)) - 1;

        printWhitespaces(firstSpaces);

        List<BinaryTreeNode<T>> newNodes = new ArrayList<BinaryTreeNode<T>>();
        for (BinaryTreeNode<T> node : nodes) {
            if (node != null) {
                System.out.print(node.data);
                newNodes.add(node.left);
                newNodes.add(node.right);
            } else {
                newNodes.add(null);
                newNodes.add(null);
                System.out.print(" ");
            }

            printWhitespaces(betweenSpaces);
        }
        System.out.println("");

        for (int i = 1; i <= endgeLines; i++) {
            for (int j = 0; j < nodes.size(); j++) {
                printWhitespaces(firstSpaces - i);
                if (nodes.get(j) == null) {
                    printWhitespaces(endgeLines + endgeLines + i + 1);
                    continue;
                }

                if (nodes.get(j).left != null){
                    System.out.print("/");
                }else{
                    printWhitespaces(1);
                }

                printWhitespaces(i + i - 1);

                if (nodes.get(j).right != null){
                    System.out.print("\\");
                }else{
                    printWhitespaces(1);
                }

                printWhitespaces(endgeLines + endgeLines - i);
            }

            System.out.println("");
        }

        printNodeInternal(newNodes, level + 1, maxLevel);
    }

    /**
     * prints a given amount of spaces
     */
    private void printWhitespaces(int count) {
        for (int i = 0; i < count; i++){
            System.out.print(" ");
        }
    }

    /**
     * takes a node, and recursively determines how many levels of subnodes it
     * has
     */
    private <T> int maxLevel(BinaryTreeNode<T> node) {
        if (node == null){
            return 0;
        }

        return Math.max(maxLevel(node.left), maxLevel(node.right)) + 1;
    }

    /**
     * checks if all elements in a given list are null 
     */
    private <T> boolean isAllElementsNull(List<T> list) {
        for (Object object : list) {
            if (object != null){
                return false;
            }
        }

        return true;
    }
}
