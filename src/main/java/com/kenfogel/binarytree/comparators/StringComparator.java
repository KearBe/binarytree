
package com.kenfogel.binarytree.comparators;

import java.util.Comparator;

/**
 *
 * @author Benjamin Kearney
 */
public class StringComparator implements Comparator<String> {
    
    @Override
    public int compare(String str1, String str2){
        if (str1 == str2) {
            return 0;
        }
        if (str1 == null) {
            return -1;
        }
        if (str2 == null) {
            return 1;
        }
        return str1.compareTo(str2);
    }
}
