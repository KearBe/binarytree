
package com.kenfogel.binarytree.comparators;

import java.util.Comparator;

/**
 *
 * @author Benjamin Kearney
 */
public class IntComparator implements Comparator<Integer> {

    @Override
    public int compare(Integer v1, Integer v2) {
        return v1 < v2 ? -1 : v1 > v2 ? +1 : 0;
    }
}
