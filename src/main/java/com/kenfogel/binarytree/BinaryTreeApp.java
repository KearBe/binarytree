package com.kenfogel.binarytree;

import com.kenfogel.binarytree.implementation.BinaryTree;
import com.kenfogel.binarytree.comparators.*;
import java.util.Comparator;

/**
 * Based on the code found at http://cslibrary.stanford.edu/110/BinaryTrees.html
 *
 * @author Ken Fogel
 */
public class BinaryTreeApp {

    private BinaryTree<Integer> tree;
    private BinaryTree<String> tree2;

    /**
     * Run the various methods in a tree to test it.
     */
    public void perform() {
        Comparator ic = new IntComparator();
        tree = new BinaryTree<Integer>(ic);
        Integer[] data = {4, 2, 8, 9, 1, 3, 7, 5, 6, 0};
        buildATree(data, tree);
        
        Comparator sc = new StringComparator();
        tree2 = new BinaryTree<String>(sc);
        String[] data2 = {"b","da", "t", "c", "W", "B"};
        buildATree(data2, tree2);
        
        System.out.println("Look up 7 = " + tree.lookup(7));
        System.out.println("Look up 10 = " + tree.lookup(10));
        System.out.println("Size of tree = " + tree.size());
        System.out.println("Max depth = " + tree.maxDepth());
        System.out.println("Min value = " + tree.minValue());
        System.out.print("Inorder = ");
        tree.printInorderTree();
        System.out.print("Postorder = ");
        tree.printPostorder();
        System.out.println("The Paths");
        tree.printPaths();
        System.out.println("The Tree");
        tree.printLineByLine();
        
        tree2.printLineByLine();
    }

    /**
     * Build a tree by inserting the members of the array into the tree.
     *
     * @param data
     */
    public <T> void buildATree(T[] data, BinaryTree<T> tree) {
        for (T element : data) {
            tree.insert(element);
        }
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        BinaryTreeApp bta = new BinaryTreeApp();
        bta.perform();
        System.exit(0);
    }
}
